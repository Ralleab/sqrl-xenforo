<?php

namespace Sqrl\Extend\XF\Admin\Controller;

use XF\Mvc\ParameterBag;

class User extends XFCP_User {
    public function actionDisassociateSqrl(ParameterBag $params)
    {
        $user = $this->assertUserExists($params->user_id);

        $this->assertCanEditUser($user);

        if ($this->isPost())
        {
            $redirect = $this->getDynamicRedirectIfNot(
                $this->buildLink('users/edit', $user),
                $this->buildLink('users/list')
            );

            $connectedAccount = $user->ConnectedAccounts['sqrl'];

            \Sqrl\Util::disassociateSqrl($connectedAccount->provider_key, $this, $connectedAccount);

            return $this->redirect($redirect);
        }
        else
        {
            $viewParams = [
                'user' => $user
            ];
            return $this->view('Sqrl:User\Disassociate', 'user_disassociate', $viewParams);
        }
    }

    
}